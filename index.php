<?php

include_once 'Model\LibriModel.php';

use libreria\LibriModel;


$model = new LibriModel();

// trovare un libro con autore sconosciuto
var_dump($model->findOneBy(['autore' => 'unknown']));

// trovare tutti libri con autore sconosciuto
var_dump($model->findBy(['autore' => 'unknown']));

// trovare tutti i libri
var_dump($model->findAll());

