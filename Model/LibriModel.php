<?php

namespace libreria;

include_once 'AbstractModel.php';
include_once 'Entity/libro.php';

use libreria\AbstractModel;

class LibriModel extends AbstractModel
{
    /**
     * @var string $tableName
     */
    protected static string $tableName = 'libri';

    /**
     * @var string $entityClass
     */
    protected static string $entityClass = libro::class;

    /**
     * @var array $primaryKeys
     */
    protected static array $primaryKeys;

    /**
     * LibriModel constructor.
     */
    public function __construct()
    {
        parent::__construct();
    }
}
