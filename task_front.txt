1 - implementare homepage base
  - implementare modello MVC generico
  - implementare API libri e categorie
		restituisce i libri dal db in base ai filtri
		restituisce tutte le categorie
		
		formato richieste:
		$_GET["request"] = "homepage"
		$_GET["request"] = "categoria", $_GET["value"] = [1, 2]
		
2 - implementare richiesta homepage
	
	nome file: homepage.html
	pagina script php: index.php
	contenuto: menu filtri e layout pagina (vedi schema)

	richieste ajax (GET):
		request: homepage
		url: index.php

	return ajax: informazioni libri da impaginare nel layout
	formato dati:

	{
		"libri":[
			{
				"id":1,
				"titolo":"Harry Potter e la pietra filosofale",
				"autore":"J. K. Rowling",
				"nazione":"Inghilterra",
				"lingua":"inglese",
				"pagine":150,
				"anno":1998,
				"copertina":"harry-potter-1.jpg",
				"prezzo":12.50,
				"sconto":20
			},
			{
				"id":2,
				"titolo":"Harry Potter e la camera dei segreti",
				"autore":"J. K. Rowling",
				"nazione":"Inghilterra",
				"lingua":"inglese",
				"pagine":174,
				"anno":2000,
				"copertina":"harry-potter-2.jpg",
				"prezzo":13.00,
				"sconto":0
			}
		],
		"categorie":[
			{
				"id":1,
				"tipo":"Architettura"
			},
			{
				"id":2,
				"tipo":"Arte"
			},
			{
				"id":3,
				"tipo":"Cucina"
			},
			{
				"id":4,
				"tipo":"Ecc"
			}
		]
	}

3 - implementare dettaglio libro nella homepage

	nome file: homepage.html
	pagina script php: index.php
	contenuto: immagine e informazioni dettagliate del libro (vedi schema)